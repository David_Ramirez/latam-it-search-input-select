import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatamItSearchInputSelectComponent } from './latam-it-search-input-select.component';

describe('LatamItSearchInputSelectComponent', () => {
  let component: LatamItSearchInputSelectComponent;
  let fixture: ComponentFixture<LatamItSearchInputSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatamItSearchInputSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatamItSearchInputSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
