import { Component, OnInit } from '@angular/core';
import { NgSelectConfig } from '../latam-it-search-input-select/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  selectedCity: any;
  selected: any;
  cities = [
    { id: 1, name: 'Vilnius', disabled: true},
    { id: 2, name: 'Kaunas'},
    { id: 3, name: 'Pabradė'},
    { id: 4, name: 'David' },
    { id: 5, name: 'Mauricio' },
    { id: 6, name: 'Pablo' },
    { id: 7, name: 'Alejandro' }
  ];

  constructor(config: NgSelectConfig) {
      config.notFoundText = 'Custom not found';
  }
  ngOnInit(): void {
    this.selected = this.cities[0].name;
  }
}
