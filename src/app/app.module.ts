import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { LatamItSearchInputSelectModule } from '../latam-it-search-input-select/latam-it-search-input-select.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule.withServerTransition({ appId: 'latam-it-search-input-select/' }),
    LatamItSearchInputSelectModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
